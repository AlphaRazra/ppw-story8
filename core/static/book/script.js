$(document).ready(function(){
    $("#formsearch").change(function(){
        search(document.getElementById("formsearch").value);
    });
}); 

function search(toSearch) {
    var searchURL = "https://www.googleapis.com/books/v1/volumes?q=" + toSearch;
    var val = "";
    var numberItems = 0;
    var count = "";
    $.ajax({
        url : searchURL,
        success : function(data){
            $("#result").empty();

            val += "<table class='table table-responsive table-striped' border=2>";
            val += "<tr>"
            val += "<th style='width: 4%'>No</th>";
            val += "<th style='width: 24%'>Title</th>";
            val += "<th style='width: 24%'>Foto Buku</th>";
            val += "<th style='width: 24%'>Penerbit</th>"
            val += "<th style='width: 24%'>Penulis</th>";
            val += "</tr>";
            $.each(data.items, function(i, item){
                numberItems = numberItems + 1;
                val += "<tr><td>" + (i + 1) + "</td>";
                val += "<td>" +item.volumeInfo.title+"</td>";
                if(item.volumeInfo.imageLinks){
                    if(!item.volumeInfo.imageLinks.smallThumbnail){
                        if(!item.volumeInfo.imageLinks.thumbnail){
                            if(!item.volumeInfo.imageLinks.medium){
                                val += "<td>Tidak Ada Foto Tersedia</td>";
                            }else{
                                val += "<td><img src=" + item.volumeInfo.imageLinks.medium + "></td>";    
                            }    
                        }else{
                            val += "<td><img src=" + item.volumeInfo.imageLinks.thumbnail + "></td>";
                        }
                    }else{
                        val += "<td><img src=" + item.volumeInfo.imageLinks.smallThumbnail + "></td>";
                    }
                }
                else{
                    val += "<td>Tidak Ada Foto Tersedia</td>";
                }

                if(item.volumeInfo.publisher){
                    val += "<td>" + item.volumeInfo.publisher +  "</td>";
                }else{
                    val += "<td>Tidak Ada Informasi Penerbit</td>";
                }
                
                if(!item.volumeInfo.authors){
                    val += "<td>Tidak Ada Informasi Penulis</td>";
                }
                else{
                    val += "<td>";
                    $.each(item.volumeInfo.authors, function(j, author){
                        val += author + "<br>";
                    });
                    val += "</td>";
                }
            });
            val += "</tr></table>"
            
            if(data.items){
                count += "<h3>" + numberItems + " Hasil Teratas:</h3>"
                $("#result").append(count);
                $("#result").append(val);
            }else{
                count += "<h3>Tidak Ditemukan Hasil</h3>"
                $("#result").append(count);
            }
            
        },
    });
}
